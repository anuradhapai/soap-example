export class HelloService {

    public hello(args: any) {
        return "Hello, " + args.name;
    }
}