import * as express from "express";
import * as bodyParser from "body-parser";
import { HelloService } from "./src/services/HelloService";

const soap = require("./App");

const app = express();
const router = express.Router();

const xml = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"><soapenv:Header/><soapenv:Body>Some Data</soapenv:Body></soapenv:Envelope>";

// body parser middleware are supported (optional)
app.use(bodyParser.raw({ type: function () { return true; }, limit: "5mb" }));
app.use("/", router);

app.listen(8080, function () {
    // Note: /wsdl route will be handled by soap module
    // and all other routes & middleware will continue to work
    soap.listen(app, "/", HelloService, xml, function () {
        console.log("server initialized");
    });
});

router.get("/hello", (request: express.Request, response: express.Response) => {
    console.log(request.method, request.path);
    response.send("hello world!");
});

router.post("/hello", (request: express.Request, response: express.Response) => {
    console.log(request.method, request.path);
    request.accepts("application/xml");
    response.send("hello world!");
});